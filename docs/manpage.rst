===========
membernator
===========

----------------------------------------------------------------
GUI tool to scan membership cards to establish if they are valid
----------------------------------------------------------------

:Author: Louis-Philippe Véronneau
:Date: 2019
:Manual section: 1

Synopsis
========

| membernator [**options**] **--database** *FILE*
| membernator (**-h** \| **--help**)
| membernator **--version**

Description
===========

**membernator** is a tool that can be used to scan membership cards and
establish if they're valid or not against a CSV database.

Options
=======

| **-h** **--help**      Shows the help screen
| **--version**          Outputs version information
| **--database** *FILE*  Path to the CSV database
| **--id_col** *ID*      "id" column in the CSV database. [default: ID]
| **--name_col** *NAME*  "name" column in the CSV database. [default: NAME]
| **--time** *SEC*       Delay in secs between scans. [default: 2.5]
| **--width** *WIDTH*    Width in pixels. Use 0 for fullscreen. [default: 800]
| **--height** *HEIGHT*  Height in pixels. Use 0 for fullscreen. [default: 480]
| **--logfile** *LOG*    Path to the logfile. [default: log.csv]

Examples
========

    $ membernator --database members.csv --width 0 --height 0

Bugs
====

Bugs can be reported to your distribution's bug tracker or upstream
at https://gitlab.com/baldurmen/membernator/issues.
