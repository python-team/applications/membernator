The manpage in this directory can be generated using the `rst2man` command
line tool provided by the Python `docutils` project:

    $ rst2man manpage.rst membernator.1
